<?php
if(file_exists('REES46/init.php')) {
    require_once('REES46/init.php');
    $view->design->assign('rees46_shop_id', REES46_SHOP_ID);
    $rees46_cart = $view->cart->get_cart();
    $view->design->assign('rees46_cart_elements', join(',', array_map(function($v){ return $v->product->id; }, $rees46_cart->purchases)));

    function get_product_categories_json_array($params, $smarty) {
        global $view;
        if(!empty($params["product"])) {
            $categories = $view->categories->get_categories(array('product_id'=>$params["product"]->id));
            if(is_array($categories)) {
                $categories = array_map(function($value){return $value->id;}, $categories);
                $categories = array_values($categories);
            } else {
                $categories = array();
            }
        } else {
            $categories = array();
        }
        return json_encode($categories);
    }

    $view->design->smarty->registerPlugin("function", "product_categories", "get_product_categories_json_array");

}
