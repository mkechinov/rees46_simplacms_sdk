<?php

header("Content-type: application/json; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

session_start();
chdir('../');
require_once('api/Simpla.php');
$simpla = new Simpla();

if(isset($_GET['variant_id'])) {
    $variant = $simpla->variants->get_variant(intval($_GET['variant_id']));
    $product = $simpla->products->get_product(intval($variant->product_id));

    $categories = $simpla->categories->get_categories(array('product_id'=>$product->id));
    if(is_array($categories)) {
        $categories = array_map(function($value){return $value->id;}, $categories);
        $categories = array_values($categories);
    } else {
        $categories = array();
    }
    $response_data = array(
        'item_id' => $product->id,
        'name' => $product->name,
        'description' => $product->description,
        'price' => $variant->price,
        'is_available' => true,
        'categories' => $categories
    );
    print json_encode($response_data);
}


