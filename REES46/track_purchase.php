<?php

$rees46_order_data = array(
    'items' => array(),
    'order_id' => $order->id
);
foreach($this->orders->get_purchases(array('order_id'=>$order->id)) as $p) {
    $rees46_order_data['items'][] = array(
        'item_id' => $p->product_id,
        'name' => $p->product_name,
        'price' => $p->price,
        'amount' => $p->amount,
        'is_available' => 1
    );
}
setcookie('rees46_track_purchase', json_encode($rees46_order_data), time() + 3600, '/');