<?php
	session_start();
	require_once('../api/Simpla.php');
	$simpla = new Simpla();

	$currencies = $simpla->money->get_currencies(array('enabled'=>1));
	if(isset($_SESSION['currency_id']))
		$currency = $simpla->money->get_currency($_SESSION['currency_id']);
	else
		$currency = reset($currencies);

	$simpla->design->assign('currency',	$currency);

    $ids = isset($_GET['ids']) && is_array($_GET['ids']) ? array_unique($_GET['ids']) : array();
    $title = isset($_GET['title']) && strlen($_GET['title']) > 0 ? $_GET['title'] : null;
    $type = isset($_GET['type']) ? $_GET['type'] : null;
    $limit = isset($_GET['limit']) ? intval($_GET['limit']) : null;

    if($ids && $type) {
        $simpla->design->assign('recommender_title', get_recommender_title($title, $type));
        $products = array();
        foreach($ids as $product_id) {
            $product = $simpla->products->get_product(intval($product_id));
            if(intval($product->visible)) {

                // Выбираем варианты товаров
                $variants = $simpla->variants->get_variants(array('product_id'=>$product->id, 'in_stock'=>true));

                // Добавляем каждый вариант к своему товару
                foreach($variants as $variant) {
                    $product->variants[] = $variant;
                }

                // Выбираем изображения товаров
                $images = $simpla->products->get_images(array('product_id'=>$product->id));
                foreach($images as $image)
                    $product->images[] = $image;

                if(isset($product->variants[0]))
                    $product->variant = $product->variants[0];
                if(isset($product->images[0]))
                    $product->image = $product->images[0];

                $products[] = $product;
            }
        }

        if($limit > 0 && count($products) > $limit) {
            $products = array_slice($products, 0, $limit);
        }

        // Если товаров больше 1 (чтобы не показывать полупустые рекомендации, то отображаем их
        if(count($products) > 1) {
            $simpla->design->assign('products', $products);
            $simpla->design->assign('type', $type);
            echo $simpla->design->fetch('rees46_recommended.tpl');
        }
    }


/**
 * Хелпер возвращает название блока рекомендованных товаров в зависимости от типа блока рекомендаций
 * @param $manual_assigned Если было передано вручную назначенное название
 * @param $recommender_type Тип рекомендера
 * @return null|string
 */
function get_recommender_title($manual_assigned, $recommender_type) {
    if($manual_assigned) {
        return $manual_assigned;
    }
    switch($recommender_type) {
        case 'popular':
            return 'Популярные товары';
            break;
        case 'interesting':
            return 'Вам будет интересно';
            break;
        case 'similar':
            return 'Похожие товары';
            break;
        case 'also_bought':
            return 'С этим товаром покупают';
            break;
        case 'see_also':
            return 'Посмотрите также';
            break;
        case 'recently_viewed':
            return 'Вы недавно смотрели';
            break;
        case 'buying_now':
            return 'Прямо сейчас покупают';
            break;
        default:
            return null;
            break;
    }
}
