<?php

$rees46_variant = $this->variants->get_variant($delete_variant_id);
if($rees46_variant) {
    setcookie('rees46_track_remove_from_cart', json_encode(array('item_id' => $rees46_variant->product_id)), time() + 3600, '/');
}
