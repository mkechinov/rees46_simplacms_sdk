{* @rees46 *}
{if $products}
    <!-- Список товаров-->
    <h2>{$recommender_title}</h2>
    <ul class="tiny_products">

        {foreach $products as $product}
            <!-- Товар-->
            <li class="product">

                <!-- Фото товара -->
                {if $product->image}
                    <div class="image">
                        <a href="products/{$product->url}?recommended_by={$type}"><img src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}"/></a>
                    </div>
                {/if}
                <!-- Фото товара (The End) -->

                <!-- Название товара -->
                <h3><a data-product="{$product->id}" href="products/{$product->url}?recommended_by={$type}">{$product->name|escape}</a></h3>
                <!-- Название товара (The End) -->


                {if $product->variants|count > 0}
                    <!-- Выбор варианта товара -->
                    <form class="variants recommended_by_rees46" action="/cart">
                        <input type="hidden" name="recommended_by" value="{$type}">
                        <table>
                            {foreach $product->variants as $v}
                                <tr class="variant">
                                    <td>
                                        <input id="featured_{$v->id}" name="variant" value="{$v->id}" type="radio" class="variant_radiobutton" {if $v@first}checked{/if} {if $product->variants|count<2}style="display:none;"{/if}/>
                                    </td>
                                    <td>
                                        {if $v->name}<label class="variant_name" for="featured_{$v->id}">{$v->name}</label>{/if}
                                    </td>
                                    <td>
                                        {if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
                                        <span class="price">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
                                    </td>
                                </tr>
                            {/foreach}
                        </table>
                        <input type="submit" class="button" value="в корзину" data-result-text="добавлено"/>
                    </form>
                    <!-- Выбор варианта товара (The End) -->
                {else}
                    Нет в наличии
                {/if}

            </li>
            <!-- Товар (The End)-->
        {/foreach}

    </ul>
{/if}